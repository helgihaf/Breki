﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Marsonsoft.Breki.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class OrdController : Controller
    {
        private readonly IOrðageymsla orðageymsla;

        public OrdController(IOrðageymsla orðaleit)
        {
            this.orðageymsla = orðaleit ?? throw new ArgumentNullException(nameof(orðaleit));
        }

        // GET api/ord/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return new ObjectResult(orðageymsla.Sækja(id));
        }
    }
}
