﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Marsonsoft.Breki.Client
{
    public partial class MainForm : Form
    {
        private Orðageymsla geymsla;

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel.Text = null;
            postWorker.RequestWork();
        }

        private void textBoxWord_TextChanged(object sender, EventArgs e)
        {
            if (textBoxWord.TextLength < 2)
            {
                return;
            }

            var leit = new Leit
            {
                Orð = textBoxWord.Text
            };
            var result = geymsla.Leita(leit);
            listView.BeginUpdate();
            listView.Items.Clear();
            if (result != null)
            {
                foreach (var orð in result)
                {
                    if (orð is Nafnorð nafnorð)
                    {
                        var listViewItem = new ListViewItem();
                        SetListViewItem(listViewItem, nafnorð);
                        listView.Items.Add(listViewItem);
                    }
                    else if (orð is Lýsingarorð lýsingarorð)
                    {
                        var listViewItem = new ListViewItem();
                        SetListViewItem(listViewItem, lýsingarorð);
                        listView.Items.Add(listViewItem);
                    }
                }
            }
            listView.EndUpdate();
            toolStripStatusLabel.Text = listView.Items.Count.ToString();
        }

        private void SetListViewItem(ListViewItem listViewItem, Nafnorð nafnorð)
        {
            listViewItem.SubItems.Clear();
            listViewItem.Text = nafnorð.Uppflettiorð;
            listViewItem.SubItems.Add("Nafnorð");
            listViewItem.SubItems.Add(nafnorð.Kyn.ToString());
            listViewItem.SubItems.Add(nafnorð.HlutiBín.ToString());
            listViewItem.SubItems.Add(nafnorð.Auðkenni.ToString());
            listViewItem.Tag = nafnorð;
        }

        private void SetListViewItem(ListViewItem listViewItem, Lýsingarorð lýsingarorð)
        {
            listViewItem.SubItems.Clear();
            listViewItem.Text = lýsingarorð.Uppflettiorð;
            listViewItem.SubItems.Add("Lýsingarorð");
            listViewItem.SubItems.Add(string.Empty);
            listViewItem.SubItems.Add(lýsingarorð.HlutiBín.ToString());
            listViewItem.SubItems.Add(lýsingarorð.Auðkenni.ToString());
            listViewItem.Tag = lýsingarorð;
        }

        private async void postWorker_DoWork(object sender, EventArgs e)
        {
            progressBar.Visible = true;
            listView.Visible = false;
            textBoxWord.Enabled = false;

            await Task.Run(() => geymsla = Orðageymsla.Load(@"..\..\..\..\data\SHSnid.csv"));

            progressBar.Visible = false;
            listView.Visible = true;
            textBoxWord.Enabled = true;
        }

        private void afritaAuðkenniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var listViewItem = listView.SelectedItems.Cast<ListViewItem>().FirstOrDefault();
            if (listViewItem != null)
            {
                var nafnorð = (Nafnorð)listViewItem.Tag;
                Clipboard.SetText(nafnorð.Auðkenni.ToString());
            }
        }
    }
}
