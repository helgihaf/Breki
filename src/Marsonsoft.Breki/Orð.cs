﻿namespace Marsonsoft.Breki
{
    public abstract class Orð
    {
        public abstract Orðflokkur Orðflokkur { get; }
        public int Auðkenni { get; set; }
        public string Uppflettiorð { get; set; }
        public HlutiBín HlutiBín { get; set; }

        internal virtual void FráOrðalínu(Orðalína orðalína)
        {
            Auðkenni = orðalína.Auðkenni;
            Uppflettiorð = orðalína.Uppflettiorð;
            HlutiBín = orðalína.HlutiBín;
        }

        internal abstract void OrðmyndFráOrðalínu(Orðalína orðalína);
    }
}
