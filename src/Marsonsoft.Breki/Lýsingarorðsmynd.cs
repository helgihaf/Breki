﻿namespace Marsonsoft.Breki
{
    public class Lýsingarorðsmynd
    {
        public string Orðmynd { get; set; }
        public Stig Stig { get; set; }
        public Ákveðni Ákveðni { get; set; }
        public Kyn Kyn { get; set; }
        public Fall Fall { get; set; }
        public Tala Tala { get; set; }
    }
}
