﻿using System.Collections.Generic;

namespace Marsonsoft.Breki
{
    public class Fall : Enumeration
    {
        private static readonly EnumerationEnumerator<Fall> enumerationEnumerator;

        public static Fall Nefnifall = new Fall(0, "NF", "Nefnifall");
        public static Fall Þolfall = new Fall(1, "ÞF", "Þolfall");
        public static Fall Þágufall = new Fall(1, "ÞGF", "Þágufall");
        public static Fall Eignarfall = new Fall(1, "EF", "Eignarfall");

        static Fall()
        {
            enumerationEnumerator = new EnumerationEnumerator<Fall>();
        }

        public Fall(int id, string name, string description)
            : base(id, name, description)
        {
        }

        public static IEnumerable<Fall> Values => enumerationEnumerator.Values;

        public static Fall Parse(string value)
        {
            return enumerationEnumerator.Parse(value);
        }
    }
}
