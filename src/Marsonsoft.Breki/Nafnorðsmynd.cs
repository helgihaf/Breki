﻿namespace Marsonsoft.Breki
{
    public class Nafnorðsmynd
    {
        public string Orðmynd { get; set; }
        public Tala Tala { get; set; }
        public Fall Fall { get; set; }
        public bool Greinir { get; set; }
    }
}
