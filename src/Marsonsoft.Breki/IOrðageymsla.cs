﻿using System.Collections.Generic;

namespace Marsonsoft.Breki
{
    public interface IOrðageymsla
    {
        IEnumerable<Orð> Leita(Leit leit);
        Orð Sækja(int auðkenni);
        IEnumerable<Orð> Sækja(string uppflettorð);
    }
}