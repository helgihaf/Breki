﻿using System;
using System.Globalization;

namespace Marsonsoft.Breki
{
    // http://bin.arnastofnun.is/gogn/snidid/

    internal class Orðaþáttari
    {
        private readonly CultureInfo cultureInfo;

        public Orðaþáttari(CultureInfo cultureInfo)
        {
            this.cultureInfo = cultureInfo ?? throw new ArgumentNullException(nameof(cultureInfo));
        }

        public Orðalína Þátta(string line)
        {
            string[] parts = line.Split(';');
            Orðalína orðalína = null;
            switch (parts[2])
            {
                case "hk":
                case "kk":
                case "kvk":
                    orðalína = ÞáttaNafnorð(parts);
                    break;
                case "lo":
                    orðalína = ÞáttaLýsingarorð(parts);
                    break;
            }

            if (orðalína == null)
            {
                return null;
            }

            orðalína.Uppflettiorð = parts[0];
            orðalína.Auðkenni = int.Parse(parts[1]);
            orðalína.HlutiBín = ÞáttaHlutaBín(parts[3]);
            orðalína.Orðmynd = parts[4];

            return orðalína;
        }

        private HlutiBín ÞáttaHlutaBín(string v)
        {
            if (!string.IsNullOrEmpty(v))
            {
                return HlutiBín.Parse(v);
            }
            else
            {
                return HlutiBín.Óskilgreint;
            }
        }

        private Nafnorðalína ÞáttaNafnorð(string[] parts)
        {
            var nafnorðalína = new Nafnorðalína
            {
                Kyn = Kyn.Parse(parts[2]),
            };

            var mark = parts[5];
            int index = 0;
            nafnorðalína.Fall = ÞáttaFall(mark, ref index);
            nafnorðalína.Tala = Tala.Parse(mark.Substring(index, 2));
            nafnorðalína.Greinir = mark.EndsWith("gr");

            return nafnorðalína;
        }

        private Fall ÞáttaFall(string mark, ref int index)
        {
            foreach (var fall in Fall.Values)
            {
                if (mark.Substring(index, fall.Name.Length) == fall.Name)
                {
                    index += fall.Name.Length;
                    return fall;
                }
            }

            throw new ArgumentException($"Invalid fall at index {index} in mark \"{mark}\"");
        }

        private Orðalína ÞáttaLýsingarorð(string[] parts)
        {
            var lýsingarorðalína = new Lýsingarorðalína();
            var loHlutar = parts[5].Split('-');

            int indexHlutar = 0;
            if (loHlutar.Length == 3)
            {
                ÞáttaStigÁkveðni(lýsingarorðalína, loHlutar[indexHlutar++]);
            }
            lýsingarorðalína.Kyn = Kyn.Parse(loHlutar[indexHlutar++].ToLower(cultureInfo));

            string fallTala = loHlutar[indexHlutar++];
            int index = 0;
            lýsingarorðalína.Fall = ÞáttaFall(fallTala, ref index);
            lýsingarorðalína.Tala = Tala.Parse(fallTala.Substring(index, 2));

            return lýsingarorðalína;
        }

        private static void ÞáttaStigÁkveðni(Lýsingarorðalína lýsingarorðalína, string stigÁkveðni)
        {
            if (stigÁkveðni == "MST")
            {
                lýsingarorðalína.Stig = Stig.Mið;
                lýsingarorðalína.Ákveðni = Ákveðni.Veik;
            }
            else
            {
                lýsingarorðalína.Stig = Stig.Parse(stigÁkveðni.Substring(0, 1));
                lýsingarorðalína.Ákveðni = Ákveðni.Parse(stigÁkveðni.Substring(1));
            }
        }
    }

    internal class Orðalína
    {
        public string Uppflettiorð { get; set; }
        public int Auðkenni { get; set; }
        public HlutiBín HlutiBín { get; set; }
        public string Orðmynd { get; set; }
    }

    internal class Nafnorðalína : Orðalína
    {
        public Kyn Kyn { get; set; }
        public Tala Tala { get; set; }
        public Fall Fall { get; set; }
        public bool Greinir { get; set; }
    }

    internal class Lýsingarorðalína : Orðalína
    {
        public Stig Stig { get; set; }
        public Ákveðni Ákveðni { get; set; }
        public Kyn Kyn { get; set; }
        public Fall Fall { get; set; }
        public Tala Tala { get; set; }
    }
}