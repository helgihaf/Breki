﻿using System.Collections.Generic;

namespace Marsonsoft.Breki
{
    public class Kyn : Enumeration
    {
        private static readonly EnumerationEnumerator<Kyn> enumerationEnumerator;

        public static Kyn Hvorugkyn = new Kyn(0, "hk", "Hvorugkyn");
        public static Kyn Karlkyn = new Kyn(1, "kk", "Karlkyn");
        public static Kyn Kvenkyn = new Kyn(2, "kvk", "Kvenkyn");

        static Kyn()
        {
            enumerationEnumerator = new EnumerationEnumerator<Kyn>();
        }

        public Kyn(int id, string name, string description)
            : base(id, name, description)
        {
        }
        public static IEnumerable<Kyn> Values => enumerationEnumerator.Values;

        public static Kyn Parse(string value)
        {
            return enumerationEnumerator.Parse(value);
        }
    }

}
