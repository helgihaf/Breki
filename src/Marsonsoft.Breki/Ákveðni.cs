﻿using System.Collections.Generic;

namespace Marsonsoft.Breki
{
    public class Ákveðni : Enumeration
    {
        private static readonly EnumerationEnumerator<Ákveðni> enumerationEnumerator;

        public static Ákveðni Sterk = new Ákveðni(0, "SB", "Sterk beyging");
        public static Ákveðni Veik = new Ákveðni(1, "VB", "Veik beyging");

        static Ákveðni()
        {
            enumerationEnumerator = new EnumerationEnumerator<Ákveðni>();
        }

        public Ákveðni(int id, string name, string description)
            : base(id, name, description)
        {
        }

        public static IEnumerable<Ákveðni> Values => enumerationEnumerator.Values;

        public static Ákveðni Parse(string value)
        {
            return enumerationEnumerator.Parse(value);
        }
    }
}
