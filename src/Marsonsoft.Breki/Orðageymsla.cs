﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Marsonsoft.Breki
{
    public class Orðageymsla : IOrðageymsla
    {
        private static readonly CultureInfo íslenska;
        private static readonly StringComparer íslenskurStringComparer;
        private static readonly Orðaþáttari orðaþáttari;

        private readonly IDictionary<int, Orð> orðEftirAuðkenni;
        private readonly IDictionary<string, IList<Orð>> orðEftirUppflettiorði;

        static Orðageymsla()
        {
            íslenska = new CultureInfo("is-IS");
            íslenskurStringComparer = StringComparer.Create(íslenska, true);
            orðaþáttari = new Orðaþáttari(íslenska);
        }

        public static Orðageymsla Load(string filePath)
        {
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var reader = new StreamReader(stream))
            {
                string line;
                Orð orð = null;
                var geymsla = new Orðageymsla();

                while ((line = reader.ReadLine()) != null)
                {
                    var orðalína = orðaþáttari.Þátta(line);
                    if (orðalína != null)
                    {
                        if (orð?.Auðkenni != orðalína.Auðkenni)
                        {
                            if (!geymsla.orðEftirAuðkenni.TryGetValue(orðalína.Auðkenni, out orð))
                            {
                                orð = Orðasmiðja.Smíða(orðalína);
                                geymsla.Add(orð);
                            }
                        }

                        orð.OrðmyndFráOrðalínu(orðalína);
                    }
                }

                return geymsla;
            }
        }

        public Orðageymsla()
        {
            orðEftirAuðkenni = new Dictionary<int, Orð>();
            orðEftirUppflettiorði = new Dictionary<string, IList<Orð>>(íslenskurStringComparer);
        }

        public Orð Sækja(int auðkenni)
        {
            orðEftirAuðkenni.TryGetValue(auðkenni, out Orð orð);

            return orð;
        }

        public IEnumerable<Orð> Sækja(string uppflettorð)
        {
            orðEftirUppflettiorði.TryGetValue(uppflettorð, out IList<Orð> listi);

            return listi;
        }

        public IEnumerable<Orð> Leita(Leit leit)
        {
            return
                from orð in orðEftirAuðkenni.Values
                where (leit.Orðflokkar == null || leit.Orðflokkar.Contains(orð.Orðflokkur))
                    && orð.Uppflettiorð.StartsWith(leit.Orð, true, íslenska)
                orderby orð.Uppflettiorð
                select orð;
        }

        private void Add(Orð orð)
        {
            orðEftirAuðkenni.Add(orð.Auðkenni, orð);
            if (!orðEftirUppflettiorði.TryGetValue(orð.Uppflettiorð, out IList<Orð> listi))
            {
                listi = new List<Orð>();
                orðEftirUppflettiorði.Add(orð.Uppflettiorð, listi);
            }
            listi.Add(orð);
        }
    }
}
