﻿using System.Collections.Generic;

namespace Marsonsoft.Breki
{
    public class Lýsingarorð : Orð
    {
        public override Orðflokkur Orðflokkur => Orðflokkur.Lýsingarorð;

        public IList<Lýsingarorðsmynd> Orðmyndir { get; } = new List<Lýsingarorðsmynd>();

        internal override void OrðmyndFráOrðalínu(Orðalína orðalína)
        {
            var lýsingarorðalína = (Lýsingarorðalína)orðalína;

            var lýsingarorðsmynd = new Lýsingarorðsmynd
            {
                Orðmynd = lýsingarorðalína.Orðmynd,
                Stig = lýsingarorðalína.Stig,
                Ákveðni = lýsingarorðalína.Ákveðni,
                Kyn = lýsingarorðalína.Kyn,
                Fall = lýsingarorðalína.Fall,
                Tala = lýsingarorðalína.Tala
            };
            Orðmyndir.Add(lýsingarorðsmynd);
        }
    }
}
