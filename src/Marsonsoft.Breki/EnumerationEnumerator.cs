﻿using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace Marsonsoft.Breki
{
    internal class EnumerationEnumerator<T> where T : Enumeration
    {
        private readonly IDictionary<string, T> values;

        public EnumerationEnumerator()
        {
            values = GetStaticFeildValues().ToDictionary(v => v.Name);
        }

        public IEnumerable<T> Values => values.Values;

        private static IEnumerable<T> GetStaticFeildValues()
        {
            var fields = typeof(T).GetTypeInfo().GetFields(BindingFlags.Public |
                BindingFlags.Static |
                BindingFlags.DeclaredOnly);
            foreach (var field in fields)
            {
                if (field.GetValue(null) is T locatedValue)
                {
                    yield return locatedValue;
                }
            }
        }

        public T Parse(string value)
        {
            return values[value];
        }
    }
}
