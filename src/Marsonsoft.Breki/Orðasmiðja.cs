﻿namespace Marsonsoft.Breki
{
    internal class Orðasmiðja
    {
        public static Orð Smíða(Orðalína orðalína)
        {
            Orð orð = null;

            if (orðalína is Nafnorðalína nafnorðalína)
            {
                orð = new Nafnorð();
            }
            else if (orðalína is Lýsingarorðalína lýsingarorðalína)
            {
                orð = new Lýsingarorð();
            }

            if (orð != null)
            {
                orð.FráOrðalínu(orðalína);
            }

            return orð;
        }
    }
}
