﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Breki
{
    public class Orðflokkur : Enumeration
    {
        private static readonly EnumerationEnumerator<Orðflokkur> enumerationEnumerator;

        public static Orðflokkur Óskilgreind = new Orðflokkur(0, "osk", "Óskilgreind");
        public static Orðflokkur Nafnorð = new Orðflokkur(1, "no", "Nafnorð");
        public static Orðflokkur Lýsingarorð = new Orðflokkur(2, "lo", "Lýsingarorð");
        public static Orðflokkur Sagnorð = new Orðflokkur(3, "so", "Sagnorð");
        public static Orðflokkur Atviksorð = new Orðflokkur(4, "ao", "Atviksorð");
        public static Orðflokkur Persónufornöfn = new Orðflokkur(5, "pfn", "Persónufornöfn");
        public static Orðflokkur AfturbeygtFornafn = new Orðflokkur(6, "afturbfn", "Afturbeygt fornafn");
        public static Orðflokkur ÖnnurFornöfn = new Orðflokkur(7, "fn", "Önnur fornöfn");
        public static Orðflokkur Töluorð = new Orðflokkur(8, "to", "Töluorð");
        public static Orðflokkur Greinir = new Orðflokkur(9, "gr", "Greinir");

        static Orðflokkur()
        {
            enumerationEnumerator = new EnumerationEnumerator<Orðflokkur>();
        }

        public Orðflokkur(int id, string name, string description)
            : base(id, name, description)
        {
        }

        public static IEnumerable<Orðflokkur> Values => enumerationEnumerator.Values;
    }
}
