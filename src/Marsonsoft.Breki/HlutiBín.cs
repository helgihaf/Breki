﻿using System.Collections.Generic;

namespace Marsonsoft.Breki
{
    public class HlutiBín : Enumeration
    {
        private static readonly EnumerationEnumerator<HlutiBín> enumerationEnumerator;

        public static HlutiBín Óskilgreint = new HlutiBín(0, "???", "Óskilgreint");
        public static HlutiBín AlmenntMál = new HlutiBín(1, "alm", "Almennt mál");
        public static HlutiBín ViðburðirOgHátíðir = new HlutiBín(2, "við", "Viðburðir og hátíðir");
        public static HlutiBín Eiginnöfn = new HlutiBín(3, "ism", "Eiginnöfn");
        public static HlutiBín Föðurnöfn = new HlutiBín(4, "föð", "Föðurnöfn");
        public static HlutiBín Móðurnöfn = new HlutiBín(5, "móð", "Móðurnöfn");
        public static HlutiBín Dýranöfn = new HlutiBín(6, "dyr", "Dýranöfn");
        public static HlutiBín Örnefni = new HlutiBín(7, "örn", "Örnefni");
        public static HlutiBín LandaheitiOgLandsvæði = new HlutiBín(8, "lönd", "Landaheiti og landsvæði");
        public static HlutiBín GötuOgBæjaheiti = new HlutiBín(9, "göt", "Götu- og bæjaheiti");
        public static HlutiBín FyrirtækjaOgStofnanaheiti = new HlutiBín(10, "fyr", "Fyrirtækja- og stofnanaheiti");
        public static HlutiBín Biblíumál = new HlutiBín(11, "bibl", "Biblíumál");
        public static HlutiBín Tölvuorð = new HlutiBín(12, "tölv", "Tölvuorð");
        public static HlutiBín Málfræðiorð = new HlutiBín(13, "málfr", "Málfræðiorð");
        public static HlutiBín Ffl = new HlutiBín(14, "ffl", "???");
        public static HlutiBín Erm = new HlutiBín(15, "erm", "???");
        public static HlutiBín Tón = new HlutiBín(16, "tón", "???");
        public static HlutiBín Sér = new HlutiBín(17, "sér", "???");
        public static HlutiBín Dýr = new HlutiBín(18, "dýr", "???");
        public static HlutiBín Heö = new HlutiBín(19, "heö", "???");
        public static HlutiBín Nett = new HlutiBín(20, "natt", "???");
        public static HlutiBín Ætt = new HlutiBín(21, "ætt", "???");
        public static HlutiBín Stærð = new HlutiBín(22, "stærð", "???");

        static HlutiBín()
        {
            enumerationEnumerator = new EnumerationEnumerator<HlutiBín>();
        }

        public HlutiBín(int id, string name, string description)
            : base(id, name, description)
        {
        }

        public static IEnumerable<HlutiBín> Values => enumerationEnumerator.Values;

        public static HlutiBín Parse(string value)
        {
            return enumerationEnumerator.Parse(value);
        }
    }
}
