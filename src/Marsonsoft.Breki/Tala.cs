﻿using System.Collections.Generic;

namespace Marsonsoft.Breki
{
    public class Tala : Enumeration
    {
        private static readonly EnumerationEnumerator<Tala> enumerationEnumerator;

        public static Tala Eintala = new Tala(0, "ET", "Eintala");
        public static Tala Fleirtala = new Tala(1, "FT", "Fleirtala");

        static Tala()
        {
            enumerationEnumerator = new EnumerationEnumerator<Tala>();
        }

        public Tala(int id, string name, string description)
            : base(id, name, description)
        {
        }

        public static IEnumerable<Tala> Values => enumerationEnumerator.Values;

        public static Tala Parse(string value)
        {
            return enumerationEnumerator.Parse(value);
        }
    }
}
