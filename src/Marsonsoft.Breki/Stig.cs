﻿using System.Collections.Generic;

namespace Marsonsoft.Breki
{
    public class Stig : Enumeration
    {
        private static readonly EnumerationEnumerator<Stig> enumerationEnumerator;

        public static Stig Efsta = new Stig(0, "E", "Efsta stig");
        public static Stig Mið = new Stig(1, "MST", "Miðstig");
        public static Stig Frum = new Stig(2, "F", "Frumstig");

        static Stig()
        {
            enumerationEnumerator = new EnumerationEnumerator<Stig>();
        }

        public Stig(int id, string name, string description)
            : base(id, name, description)
        {
        }

        public static IEnumerable<Stig> Values => enumerationEnumerator.Values;

        public static Stig Parse(string value)
        {
            return enumerationEnumerator.Parse(value);
        }
    }
}
