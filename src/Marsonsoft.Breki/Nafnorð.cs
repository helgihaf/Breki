﻿using System.Collections.Generic;
using System.Linq;

namespace Marsonsoft.Breki
{
    public class Nafnorð : Orð
    {
        public override Orðflokkur Orðflokkur => Orðflokkur.Nafnorð;

        public Kyn Kyn { get; set; }
        public IList<Nafnorðsmynd> Orðmyndir { get; } = new List<Nafnorðsmynd>();

        internal override void FráOrðalínu(Orðalína orðalína)
        {
            base.FráOrðalínu(orðalína);
            var nafnorðalína = (Nafnorðalína)orðalína;

            Kyn = nafnorðalína.Kyn;
        }

        internal override void OrðmyndFráOrðalínu(Orðalína orðalína)
        {
            var nafnorðalína = (Nafnorðalína)orðalína;
            var nafnorðamynd = new Nafnorðsmynd
            {
                Orðmynd = nafnorðalína.Orðmynd,
                Fall = nafnorðalína.Fall,
                Tala = nafnorðalína.Tala,
                Greinir = nafnorðalína.Greinir
            };
            Orðmyndir.Add(nafnorðamynd);
        }

        public string SækjaOrðmynd(Tala tala, Fall fall, bool greinir)
        {
            return Orðmyndir.Where(m => m.Tala == tala && m.Fall == fall && m.Greinir == greinir).FirstOrDefault()?.Orðmynd;
        }
    }
}