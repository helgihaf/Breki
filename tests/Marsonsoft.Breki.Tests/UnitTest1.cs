using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Marsonsoft.Breki.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private static IOr�ageymsla or�ageymsla;
        private static readonly object leitMonitor = new object();

        public UnitTest1()
        {
        }

        private static IOr�ageymsla Or�ageymsla
        {
            get
            {
                if (or�ageymsla == null)
                {
                    lock (leitMonitor)
                    {
                        if (or�ageymsla == null)
                        {
                            or�ageymsla = Marsonsoft.Breki.Or�ageymsla.Load(@"..\..\..\..\data\SHSnid.csv");
                        }
                    }
                }

                return or�ageymsla;
            }
        }

        [TestMethod]
        public void CreateLookup_Ok()
        {
            Assert.IsNotNull(Or�ageymsla);
        }

        [TestMethod]
        public void S�kja_Nafnord_Ok()
        {
            var nafnor� = Or�ageymsla.S�kja("sver�").Single() as Nafnor�;
            Assert.AreEqual(Kyn.Hvorugkyn, nafnor�.Kyn);
            Assert.AreEqual(HlutiB�n.AlmenntM�l, nafnor�.HlutiB�n);
            Assert.AreEqual("sver�", nafnor�.S�kjaOr�mynd(Tala.Eintala, Fall.Nefnifall, false));
            Assert.AreEqual("sver�i�", nafnor�.S�kjaOr�mynd(Tala.Eintala, Fall.Nefnifall, true));
        }

        [TestMethod]
        public void S�kja_Nafnord2_Ok()
        {
            var nafnor� = Or�ageymsla.S�kja("�tihur�").Single() as Nafnor�;
            Assert.AreEqual(Kyn.Kvenkyn, nafnor�.Kyn);
            Assert.AreEqual("�tihur�", nafnor�.S�kjaOr�mynd(Tala.Eintala, Fall.Nefnifall, false));
            Assert.AreEqual("�tihur�in", nafnor�.S�kjaOr�mynd(Tala.Eintala, Fall.Nefnifall, true));
        }

        [TestMethod]
        public void S�kja_L�singaror�_Ok()
        {
            var l�singaror� = Or�ageymsla.S�kja("gulur").Single() as L�singaror�;
            var or�mynd = l�singaror�.Or�myndir.Where(m => m.Kyn == Kyn.Kvenkyn && m.Fall == Fall.Nefnifall && m.Stig == Stig.Frum && m.�kve�ni == �kve�ni.Sterk).FirstOrDefault();
            Assert.AreEqual("gul", or�mynd.Or�mynd);
        }

        [TestMethod]
        public void S�kjaEftirAu�kenni_Nafnor�_Ok()
        {
            var or� = Or�ageymsla.S�kja(101370);
            Assert.AreEqual("uppreisnarseggur", or�.Uppflettior�);
        }

        [TestMethod]
        public void Leita_Nafnor�_Ok()
        {
            var leit = new Leit
            {
                Or�flokkar = new[] { Or�flokkur.Nafnor� },
                Or� = "uppreisn"
            };
            var or� = Or�ageymsla.Leita(leit);
            Assert.AreEqual(28, or�.Count());

        }
    }
}
