using System;
using System.Linq;
using Xunit;

namespace Marsonsoft.Breki.Tests
{
    public class UnitTest1
    {
        private Lookup lookup;

        public UnitTest1()
        {
        }

        private Lookup Lookup
        {
            get
            {
                if (lookup == null)
                {
                    lookup = Lookup.Load(@"..\..\..\..\..\data\SHSnid.csv");
                }

                return lookup;
            }
        }

        [Fact]
        public void CreateLookup_Ok()
        {
            Assert.NotNull(Lookup);
        }

        [Fact]
        public void Lookup_Nafnord_Ok()
        {
            var nafnor� = Lookup.FinnaOr�("sver�").Single() as Nafnor�;
            Assert.Equal(Kyn.Hvorugkyn, nafnor�.Kyn);
            Assert.Equal(HlutiB�n.AlmenntM�l, nafnor�.HlutiB�n);
            Assert.Equal("sver�", nafnor�.S�kjaOr�mynd(Tala.Eintala, Fall.Nefnifall, false));
            Assert.Equal("sver�i�", nafnor�.S�kjaOr�mynd(Tala.Eintala, Fall.Nefnifall, true));
        }

        [Fact]
        public void Lookup_Nafnord2_Ok()
        {
            var nafnor� = Lookup.FinnaOr�("kothur�").Single() as Nafnor�;
            Assert.Equal(Kyn.Kvenkyn, nafnor�.Kyn);
            Assert.Equal("kothur�", nafnor�.S�kjaOr�mynd(Tala.Eintala, Fall.Nefnifall, false));
            Assert.Equal("kothur�in", nafnor�.S�kjaOr�mynd(Tala.Eintala, Fall.Nefnifall, true));
        }

        [Fact]
        public void Lookup_L�singaror�_Ok()
        {
            var l�singaror� = Lookup.FinnaOr�("gulur").Single() as L�singaror�;
            var or�mynd = l�singaror�.Or�myndir.Where(m => m.Kyn == Kyn.Kvenkyn && m.Fall == Fall.Nefnifall && m.Stig == Stig.Frum && m.�kve�ni == �kve�ni.Sterk).FirstOrDefault();
            Assert.Equal("gul", or�mynd.Or�mynd);
        }
    }
}
